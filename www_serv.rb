require 'socket'
webserver = TCPServer.new('127.0.0.1', 20000)

puts "Starting ixutstats www server..."
puts "Press [Ctrl+C] to stop."

www_dir = "./www"

while (session = webserver.accept)
	session.print "HTTP/1.1 200/OK\r\nContent-type:text/html\r\n\r\n"

	begin
		request = session.gets
	rescue Errno::ECONNRESET
		session = webserver.accept
	end

	# Get filename
	trimmedrequest = request.gsub(/GET\ \//, '').gsub(/\ HTTP.*/, '')
	filename = trimmedrequest.chomp

	# Show index if file not set
	# TODO -- Check weird values
	if filename == ""
		filename = "#{www_dir}/index.html"
	else
		filename = "#{www_dir}/#{filename}"
	end

	# Render page
	begin
		renderpage = File.open(filename, 'r')
		session.print renderpage.read()
		rescue Errno::ENOENT
		session.print "ixutstats -- File not found: #{filename}"
	end

	session.close
end
