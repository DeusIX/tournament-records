# Tournament Records -- Unreal Log Parser & Database System.

![project icon](https://assets.gitlab-static.net/uploads/-/system/project/avatar/13997554/unreal.png?width=64 "icon")

Tournament Records is a log parser for Unreal logs. At the moment it's very limited and 
supports only UT2.


## [ Requirements ]
- Ruby v1.9
- Sequel.gem for ruby
- some UT2 logfiles.

## [ Changelog ]
- 2018/05/19 -- 0.0.2 Simple www-server added. Does not access databases yet.
- 2018/05/13 -- 0.0.1 Inital Commit.
