require 'sequel'
DB = Sequel.connect('sqlite://data/ixutTest1.db')
PARSE_ON = 0	# Run parser
INIT = 0		# Create Tables
INTERACTIVE = 0	# 1 = interactive, 0 = disable
require './functions.rb'


@kills         = DB[:m_kills] 		# Create a dataset - KillMatrix
@match_summary = DB[:matchdata] 	# Create a dataset - Summary
@match_stats   = DB[:match_stats] 	# Create a dataset - Stats
@items         = DB[:items] 		# Create a dataset - Items
@players       = DB[:players] 		# Create a dataset - Players

@real_items    = DB[:real_items]	# Create a dataset - Players
@map_data      = DB[:map_data] 		# Create a dataset - Map Info
@scores        = DB[:scores] 		# Create a dataset - Scores
@scores_team   = DB[:scores_team] 	# Create a dataset - Scores
$THIS_MATCH    = @match_summary.count + 1
$GAME_START    = 0.00
$GAME_END      = 0.00


#1563.22 P       0       XGame.ComboDefensive

puts ""					# Print Header
puts "+"+"".center(78,"=")+"+"
puts "|"+"IXUT RUBY STATS".center(78," ")+"|"
puts "|"+"v 0.0.4 ".rjust(78," ")+"|"
puts "+"+"".center(78,"=")+"+"
#┏━┓
#┃╋┣┫
#┗┳┛┻

DB.transaction do
if PARSE_ON == 1
puts "now parsing match ##{$THIS_MATCH}..."
f = File.open("./utlog/UT2_ONS.log") 
f.each do |line|

#File.open('bak/testQWERT.txt', 'w') do |file|
	stamp, code = line.chomp.split(/\t/) 

	if code == "NG" # NewGame
		stamp, code, date, UNK, mapfile, mapname, mapauthor, gametype, gamename, mutators = line.chomp.split(/\t/)
		# TODO -- UNK ?
		@match_summary.insert( :m_id => $THIS_MATCH, :date => date,:mapfile => mapfile,:mapname => mapname,:mapauthor => mapauthor,:gametype => gametype,:gamename => gamename,:mutators => mutators )
	elsif code == "SI"				# ServerInfo
		stamp, code, servername, UNK2,a,b,c, options = line.chomp.split(/\t/) # ignore abc
		# TODO -- UNK2 ?
		DB.run("UPDATE matchdata SET servername=\"#{servername}\" WHERE m_id = \"#{$THIS_MATCH}\" ")
		DB.run("UPDATE matchdata SET options=\"#{options}\" WHERE m_id = \"#{$THIS_MATCH}\" ")

	elsif code == "SG"				# START
		$GAME_START, code = line.chomp.split(/\t/)

		DB.run("UPDATE matchdata SET timestart=\"#{$GAME_START}\" WHERE m_id = \"#{$THIS_MATCH}\" ")

	elsif code == "EG"				# END			212.77	EG	serverquit	57	58	59
									#				4651.98	EG	teamscorelimit	0	6	13	16	12	8	5	10	17	3	11	7	14	21	4	20	9	15
		$GAME_END, code, reason  = line.chomp.split(/\t/)
		DB.run("UPDATE matchdata SET timeend=\"#{$GAME_END}\" WHERE m_id = \"#{$THIS_MATCH}\" ")
		# TODO -- set times for players
		puts $GAME_END, code, reason
		endgame_playertimes


	elsif code == "G" or code == "C" or code == "BI" or code == "PS"	# PlayerData -- TODO C+BI+PS
		if line =~ /NameChange/				# NEW PLAYER
			line = line.sub(/\[BOT\]/, '') 	# rm [BOT]
			stamp, code, type, id, name = line.chomp.split(/\t/)
			if id != "-1"	# sometimes -1 appears ?

			# TODO -- add matchcount
			mypid = getPID(name)	# NEW CONTENDER -> get PID
				if mypid == 0 			# NEW CONTENDER -> New PID
					mypid = @players.count+1
					@players.insert( :p_id => mypid, :p_name => name, :first_game => $THIS_MATCH, :last_game => $THIS_MATCH, :score => 0, :frags => 0, :kills => 0, :deths => 0, :suics => 0, :teamk => 0, :teamd => 0, :team => -1, :matches => 1, :time => 0 ) # 
				else
					# TODO -- Need to check if the player has joined the game before or changed team
					DB.run("UPDATE players SET matches=matches+1, last_game=\"#{$THIS_MATCH}\" WHERE p_id=\"#{mypid}\" ")	
				end

			pid_append(id,mypid)
			@match_stats.insert( :m_id => $THIS_MATCH, :join => stamp, :p_id => mypid, :p_name => name,:part => -1, :score => 0, :frags => 0, :kills => 0, :deths => 0, :suics => 0, :teamk => 0, :teamd => 0, :team => -1, :time => 0 ) #TODO --TEST

			end

		elsif line =~ /TeamChange/	# TEAM					//7.22	G	TeamChange	58	0
			# TODO -- Handle team changes
			stamp, code, type, id, team = line.chomp.split(/\t/)
			id = pid_loc2rea(id)
			DB.run("UPDATE match_stats SET team=\"#{team}\" WHERE m_id = \"#{$THIS_MATCH}\" AND p_id=\"#{id}\" ")
		end


	elsif code == "I"				# ITEM PICKUP			//9.33	I	59	Flare
		stamp, code, id, item = line.chomp.split(/\t/)
		# TODO -- TEST temp table for 
		id = pid_loc2rea(id)
		@items.insert( :m_id => $THIS_MATCH, :stamp => stamp, :p_id => id,  :item => item ) 	# TODO --TEST :p_name => name,
		#if :item exists -> get :id
		#else add new -> 
		#real_items :id :item	FIXME -- is done?

	elsif code == "K"				# KILL & DEATH
		stamp, code, idK, kw, idD, dw = line.chomp.split(/\t/)
		idK = pid_loc2rea(idK)
		idD = pid_loc2rea(idD)
		if idK == idD or idK == 0 # suic
			DB.run("UPDATE match_stats SET suics=suics+1 WHERE m_id = \"#{$THIS_MATCH}\" AND p_id=\"#{idK}\" ") # 
			DB.run("UPDATE players SET suics=suics+1 WHERE p_id=\"#{idK}\" ") 									# 
			@kills.insert(:m_id => $THIS_MATCH, :stamp => stamp, :killer => idD, :victim => idD, :k_weap => kw, :v_weap => dw )			# 
		else
			DB.run("UPDATE match_stats SET kills=kills+1 WHERE m_id = \"#{$THIS_MATCH}\" AND p_id=\"#{idK}\" ")	# 
			DB.run("UPDATE match_stats SET deths=deths+1 WHERE m_id = \"#{$THIS_MATCH}\" AND p_id=\"#{idD}\" ")	# 
			DB.run("UPDATE players SET kills=kills+1 WHERE p_id=\"#{idK}\" ")									# 
			DB.run("UPDATE players SET deths=deths+1 WHERE p_id=\"#{idD}\" ")									# 
			@kills.insert(:m_id => $THIS_MATCH, :stamp => stamp, :killer => idK, :victim => idD, :k_weap => kw, :v_weap => dw )			# 
		end


	elsif code == "TK"				# TEAM KILL				//62.50   TK      57      DamTypeRedeemer 0       None
		stamp, code, idK, weap, idD, dw = line.chomp.split(/\t/)
		idK = pid_loc2rea(idK)
		idD = pid_loc2rea(idD)
		DB.run("UPDATE match_stats SET teamk=teamk+1 WHERE m_id = \"#{$THIS_MATCH}\" AND p_id=\"#{idK}\" ")		#
		DB.run("UPDATE match_stats SET teamd=teamd+1 WHERE m_id = \"#{$THIS_MATCH}\" AND p_id=\"#{idD}\" ")		# 
		DB.run("UPDATE players SET teamk=teamk+1 WHERE p_id=\"#{idK}\" ")										# TODO -- TEST
		DB.run("UPDATE players SET teamd=teamd+1 WHERE p_id=\"#{idD}\" ")										# 

	elsif code == "MD"				# MONSTER KILLED		//119.91	MD	58	DamTypeShieldImpact	Krall4
		stamp, code, id, weap, monster, = line.chomp.split(/\t/)
		id = pid_loc2rea(id)
		DB.run("UPDATE match_stats SET kills=kills+1 WHERE m_id = \"#{$THIS_MATCH}\" AND p_id=\"#{id}\" ")		# MH kills as K
		DB.run("UPDATE players SET kills=kills+1 WHERE p_id=\"#{id}\" ")										# MH kills as K

	elsif code == "S"				# SCORE					//61.39	S	57	1.00	invasion_frag
		stamp, code, id, scor, type = line.chomp.split(/\t/)
		id = pid_loc2rea(id)
		unless id == 0
			DB.run("UPDATE match_stats SET score=score+\"#{scor}\" WHERE m_id = \"#{$THIS_MATCH}\" AND p_id=\"#{id}\" ") 
			DB.run("UPDATE players SET score=score+\"#{scor}\" WHERE p_id=\"#{id}\" ")									# 
			# TODO -- SCORE DATABASE TABLE -> graph plotting
			@scores.insert( :m_id => $THIS_MATCH, :stamp => stamp, :p_id => id, :score => scor, :types => type ) #TODO --TEST
		end
	elsif code == "T"				# TEAM SCORE			//119.91	T	0	2.00	invasion_frag
		stamp, code, id, score, type = line.chomp.split(/\t/)
		# TODO -- TEAM DATABASE TABLE
		@scores_team.insert( :m_id => $THIS_MATCH, :stamp => stamp, :t_id => id, :score => scor, :types => type ) #TODO --TEST

	elsif code == "PP"				# PING					//120.01	PP	57	0

	elsif code == "PA"				# WEAPON DATA			//4651.98	PA	0	ShockRifle	107	11	612
		stamp, code, id, weap, shots,hits,dmg = line.chomp.split(/\t/)

	elsif code == "P"				# SPREE					//1645.33 P       4       spree_1
		stamp, code, id, type = line.chomp.split(/\t/)

	unless line =~ /OverloadUT www.Apartment167.com/
		puts line
	end




	end
end
f.close	#-----------------------------------------------------
end


end

#@match_summary.where(Sequel[:m_id] > 5).delete
#@match_summary.where(Sequel[:m_id] != 2 ).update(:servername => 'SRAA')
#@match_summary.select(:m_id)

#puts "match_summary played: #{@match_summary.count} = #{$THIS_MATCH}"
#@match_summary.each{|row| 
#	puts row
#}


#thisID = 1
#puts @match_summary.where(:id => thisID).get(:date)
#puts @match_summary.where(:id => thisID).get(:servername)

#my_posts = @match_summary.where{ m_id > 16 }.get(:date)
#my_posts.each{|row| p row}
#puts my_posts

#puts "==<1>=="
#mypid = getPID("Zakoi")
#map_pid(57)
#puts "==<2>=="
#mypid = getPID("Bob")
#puts "==<Z>=="
#puts ""


@matchCount = @match_summary.count


def output_latest_matches(type="term")
	# print latest 5 matches
	mymatchCount = @matchCount
	mylimit = 5

	latestmatch_summary = @match_summary.where{ m_id > (mymatchCount - mylimit) }

	case 	# select type to format
	when type == "term"
		puts "== match_summary =="
		latestmatch_summary.each{|row| 
			puts ">> ##{row[:id]}) #{row[:date]} | #{row[:gamename]} @ #{row[:mapname]} (#{row[:timestart]}-#{row[:timeend]}) (#{row[:servername]})"
		}


	when type == "write"
		puts "writing latest matches ..."

		File.open("www/latest.html", 'w') do |file|

		file.puts "<html>"
		file.puts "<head><title> ixutstats </title></head>"
		file.puts "<body>"
		file.puts ""
		file.puts "<h1><a href=\"./index.html\">ixutstats</a></h1>"
		file.puts "<h2>&gt;Latest Matches</h2>"

		file.puts "<table>"
		file.puts "<tr><td>#</td> <td>Date</td> <td>Map</td> <td>Server</td> </tr>"

		latestmatch_summary.each{|row| 
			file.puts "<tr> <td>#{row[:id]}</td> <td>#{row[:date]} <td><a href=\"./matches/#{row[:id]}.html\"> #{row[:gamename]} @ #{row[:mapname]} </a></td> <td>(#{row[:servername]})</td></tr>"
		}
		file.puts "</table>"
		file.puts "</body>"
		file.puts "</html>"

		end
	end
end


def output_total_kills
	# print total kills
	puts "== KILLS =="
	@kills.each{|row| 
		stamp  = row[:stamp]
		killer = playerdata(row[:killer]).red
		victim = playerdata(row[:victim])
		k_weap = weapondata(row[:k_weap])
		v_weap = weapondata(row[:v_weap])
		puts "@#{stamp} #{killer}(#{k_weap}) -> #{victim}(#{v_weap})"
	}
end


def output_total_players
	puts "== PLAYER LIST =="
	puts "name".ljust(12)+"\tscore\tfrag\tkill\tdeath\tsuic\ttk\ttd\tteam\tmatches"
	@players.each{|row| 
		id = row[:id]
		p_id = row[:p_id]
		name = row[:p_name].to_s.ljust(12)
		stat_q = row[:score]
		stat_k = row[:kills]
		stat_d = row[:deths]
		stat_s = row[:suics]
		stat_tk = row[:teamk]
		stat_td = row[:teamd]
		team = row[:team]
		matches = row[:matches]
		game_a = row[:first_game] 
		game_z = row[:last_game]
		stat_f = stat_k - (stat_s + stat_tk) # row[:frags]	# TODO -- RM FFRAGS
		effic = format_float(stat_k.to_f/stat_d)
		if effic.to_f > 2
			effic = effic.green
		elsif effic.to_f < 0.5
			effic = effic.red
		end
# TODO -- GET TIMES
		puts "(#{id} #{p_id}) #{name}#{stat_q}\t#{stat_f}\t#{stat_k}\t#{stat_d}\t#{stat_s}\t#{stat_tk}\t#{stat_td}\t#{team}\t#{matches}\t#{effic}"
	}
end




def output_total_matchdata
	puts "== MATCHDAAT =="
	puts "name".ljust(12)+"\tstat_q\tstat_f\tstat_k\tstat_d\tstat_s\tstat_tk\tstat_td\tteam"
	@match_stats.each{|row| 
		id = row[:id]
		m_id = row[:m_id]
		name = row[:p_name].to_s.ljust(12)
		stat_q = row[:score]
		#stat_f = row[:frags]
		stat_k = row[:kills]
		stat_d = row[:deths]
		stat_s = row[:suics]
		stat_tk = row[:teamk]
		stat_td = row[:teamd]
		team = row[:team]
		stat_f = stat_k - (stat_s + stat_tk) # row[:frags]	# TODO -- RM FFRAGS
		puts "(#{m_id}) #{name}#{stat_q}\t#{stat_f}\t#{stat_k}\t#{stat_d}\t#{stat_s}\t#{stat_tk}\t#{stat_td}\t#{team}"
	}
end

def output_total_items
	puts "== ITEMS =="
	@items.each{|row| 
		id = row[:id]
		stamp = row[:stamp]
		m_id = row[:m_id]
		p_id = row[:p_id]
		item = row[:item]
		puts "#{stamp}\tm#{m_id}\tp#{p_id}\t#{item}"
	}
end


def output_total_score
	puts "== SCORES =="
	@scores.each{|row| 
		m_id = row[:m_id]
		stamp = row[:stamp]
		p_id = row[:p_id]
		score = row[:score]
		types = row[:types]
		puts "#{stamp}\tm#{m_id}\tp#{p_id}\t#{score}\t#{types}"
	}
end



def output_total_teamscore
	puts "== TEAM SCORES =="
	@scores_team.each{|row| 
		m_id = row[:m_id]
		stamp = row[:stamp]
		t_id = row[:t_id]
		score = row[:score]
		types = row[:types]
		puts "#{stamp}\tm#{m_id}\tp#{t_id}\t#{score}\t#{types}"
	}
end








def output_match(matchID,type="term")

	puts "TYPE: #{type}"
	case 	# select type to format
		when type == "term"

		puts "== MATCH #{matchID} ==".yellow.bold
		puts @match_summary.where(:id => matchID).get(:date)
		puts @match_summary.where(:id => matchID).get(:servername)

		puts "name".ljust(12)+"sc\tF\tK\tD\tS\tTK\tTD\tTeam"


		@match_stats.each{|row| 
		if row[:m_id] == matchID 
			m_id = row[:m_id]
			name = row[:p_name].to_s.ljust(12)
			join = row[:join]
	part = row[:part]
			stat_q = row[:score]
			stat_k = row[:kills]
			stat_d = row[:deths]
			stat_s = row[:suics]
			stat_tk = row[:teamk]
			stat_td = row[:teamd]
			team = row[:team]
			stat_f = stat_k - (stat_s + stat_tk) # row[:frags]	# TODO -- RM FFRAGS
			puts "#{name}".ljust(12)+"#{stat_q}\t#{stat_f}\t#{stat_k}\t#{stat_d}\t#{stat_s}\t#{stat_tk}\t#{stat_td}\t#{team} * #{join} -> #{part}"
		end
		}

	when type == "write"
		puts "write match..."

		# create directory for match details
		myDir = "./www/matches/#{matchID}"
		if !Dir.exists?(myDir)
			Dir.mkdir(myDir)
		end
		File.open("www/matches/#{matchID}.html", 'w') do |file|
		file.puts "<html>"
		file.puts "<head><title> ixutstats </title></head>"
		file.puts "<body>"
		file.puts ""
		file.puts "<h1><a href=\"../index.html\">ixutstats</a></h1>"
		file.puts "<h2><a href=\"../latest.html\">&gt;Latest Matches</a></h2>"
		file.puts "<h3>&gt;&gt;Match #{matchID} </h3>"
		file.puts "<table>"
		file.puts "<tr><td>#{@match_summary.where(:id => matchID).get(:date)}</td></tr>"
		file.puts "<tr><td>#{@match_summary.where(:id => matchID).get(:servername)}</td></tr>"
		file.puts "</table>"

		file.puts "<table>"
		file.puts "<tr><td>Name</td> <td>Score</td> <td>F</td> <td>K</td> <td>D</td> <td>S</td> <td>TK</td> <td>TD</td> <td>Team</td></tr>"

		@match_stats.each{|row| 
		if row[:m_id] == matchID
			m_id = row[:m_id]
			p_id = row[:p_id]
			name = row[:p_name].to_s.ljust(12)
			stat_q = row[:score]
			stat_k = row[:kills]
			stat_d = row[:deths]
			stat_s = row[:suics]
			stat_tk = row[:teamk]
			stat_td = row[:teamd]
			team = row[:team]
			stat_f = stat_k - (stat_s + stat_tk)

			file.puts "<tr><td><a href=\"./#{matchID}/#{p_id}.html\">#{name}</a></td> <td>#{stat_q}</td> <td>#{stat_f}</td> <td>#{stat_k}</td> <td>#{stat_d}</td> <td>#{stat_s}</td> <td>#{stat_tk}</td> <td>#{stat_td}</td> <td>#{team}</td></tr>"
			output_match_detail(m_id,p_id,"write")
		end
		}
		file.puts "</table>"

		end
	end
end







def output_match_detail(matchID,playerID,type="term")

	# Get Detail data
	my_itemhash = Hash.new(0)	# Items
	@items.each{|row| 
	if row[:m_id] == matchID and row[:p_id] == playerID
		stamp = row[:stamp]
		m_id = row[:m_id]
		p_id = row[:p_id]
		item = row[:item]
		my_itemhash[item] += 1
	end
	}

	my_killhash = Hash.new(0)	# Kills
	@kills.each{|row|
	if row[:m_id] == matchID and row[:killer].to_i == playerID.to_i
		stamp  = row[:stamp]
		killer = playerdata(row[:killer])
		victim = playerdata(row[:victim])
		k_weap = weapondata(row[:k_weap])
		v_weap = weapondata(row[:v_weap])
		my_killhash[victim] += 1
	end
	}

	my_deathhash = Hash.new(0)	# Deaths
	@kills.each{|row|
	if row[:m_id] == matchID and row[:victim].to_i == playerID.to_i
		stamp  = row[:stamp]
		killer = playerdata(row[:killer])
		victim = playerdata(row[:victim])
		k_weap = weapondata(row[:k_weap])
		v_weap = weapondata(row[:v_weap])
		my_deathhash[killer] += 1
	end
	}


case 	# select type to format
	when type == "term"
	puts "".center(80,"_")
	puts "=== MATCH #{matchID} ===".center(80,"= ")
	puts @match_summary.where(:id => matchID).get(:date)
	puts @match_summary.where(:id => matchID).get(:servername)

	puts "name".ljust(12)+"sc\tF\tK\tD\tS\tTK\tTD\tTeam"
	@match_stats.each{|row| 
	if row[:m_id] == matchID and row[:p_id] == playerID
		m_id = row[:m_id]
		name = row[:p_name].to_s.ljust(12)
		stat_q = row[:score]
		stat_k = row[:kills]
		stat_d = row[:deths]
		stat_s = row[:suics]
		stat_tk = row[:teamk]
		stat_td = row[:teamd]
		team = row[:team]
		stat_f = stat_k - (stat_s + stat_tk) # row[:frags]	# TODO -- RM FFRAGS
		puts "#{name}".ljust(12)+"#{stat_q}\t#{stat_f}\t#{stat_k}\t#{stat_d}\t#{stat_s}\t#{stat_tk}\t#{stat_td}\t#{team}"
	end
	}


	# print items
	puts "== ITEMS ==".center(80,"= ")
	puts "Item ".ljust(32,"_")+"Amount"
	my_itemhash.each do |i|
		puts "#{i.fetch(0)}".ljust(32)+"#{i.fetch(1)}"
	end


	# print kills
	puts "== KILLS v DEATHS ==".center(80,"= ")
	puts "Opponent".ljust(12,"_")+"K".rjust(4)+" v D".ljust(6)+"Ratio"
	my_killhash.each do |v|
		my_ratio = format_float(v.fetch(1).to_f / my_deathhash.fetch(v.fetch(0)).to_f)
		puts "#{v.fetch(0)}".ljust(12)+"#{v.fetch(1)}".rjust(4)+" v #{my_deathhash.fetch(v.fetch(0))}".ljust(6)+"#{my_ratio}"
	end





when type == "write"
	puts "write playerdetails #{playerID}..."

	File.open("www/matches/#{matchID}/#{playerID}.html", 'w') do |file|
	file.puts "<html>"
	file.puts "<head><title> ixutstats </title></head>"
	file.puts "<body>"
	file.puts ""
	file.puts "<h1><a href=\"../../index.html\">ixutstats</a></h1>"
	file.puts "<h2><a href=\"../../latest.html\">&gt;Latest Matches</a></h2>"
	file.puts "<h3><a href=\"../../matches/#{matchID}.html\">&gt;&gt;Match #{matchID}</a></h3>"
	file.puts "<h4>&gt;&gt;Match Details for Player --TODO--</h4>"
	file.puts "<table>"
	file.puts "<tr><td>#{@match_summary.where(:id => matchID).get(:date)}</td></tr>"
	file.puts "<tr><td>#{@match_summary.where(:id => matchID).get(:servername)}</td></tr>"
	file.puts "</table>"

	file.puts "<table>"
	file.puts "<tr><td>Name</td> <td>Score</td> <td>F</td> <td>K</td> <td>D</td> <td>S</td> <td>TK</td> <td>TD</td> <td>Team</td></tr>"

	@match_stats.each{|row| 
	if row[:m_id] == matchID and row[:p_id] == playerID
		m_id = row[:m_id]
		name = row[:p_name]
		stat_q = row[:score]
		stat_k = row[:kills]
		stat_d = row[:deths]
		stat_s = row[:suics]
		stat_tk = row[:teamk]
		stat_td = row[:teamd]
		team = row[:team]
		stat_f = stat_k - (stat_s + stat_tk)

		file.puts "<tr><td>#{name}</td> <td>#{stat_q}</td> <td>#{stat_f}</td> <td>#{stat_k}</td> <td>#{stat_d}</td> <td>#{stat_s}</td> <td>#{stat_tk}</td> <td>#{stat_td}</td> <td>#{team}</td></tr>"
	end
	}
	file.puts "</table>"


	file.puts "<table>"
	file.puts "<tr><td>Opponent</td><td>K</td><td> v </td><td>D</td><td>Ratio</td></tr>"
	my_killhash.each do |v|
		my_ratio = format_float(v.fetch(1).to_f / my_deathhash.fetch(v.fetch(0)).to_f)
		file.puts "<tr><td>#{v.fetch(0)}</td><td>#{v.fetch(1)}</td><td> v </td><td>#{my_deathhash.fetch(v.fetch(0))}</td><td>#{my_ratio}</td></tr>"
	end
	file.puts "</table>"


	# print items
	file.puts "<table>"
	file.puts "<tr><td>Item</td><td>Amount</td></tr>"
	my_itemhash.each do |i|
		file.puts "<tr><td>#{i.fetch(0)}</td><td>#{i.fetch(1)}</td></tr>"
	end
	file.puts "</table>"


	end # file
end # case


end # main

require './interactive.rb'


output_latest_matches
#output_total_kills
#output_latest_matches("write")
#output_match(1,"write") #,"write"
#puts "--------"
output_match(@matchCount)
#output_match_detail(1,1,"write")
#output_match_detail(1,1)
#output_total_players





