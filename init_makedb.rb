puts "=== INIT ==="
puts "creating tables..."

puts "creating table: matchdata (matchdata)..."
DB.create_table :matchdata do
	primary_key :id		# 
	Int    :m_id		# gameID
	String :date		# StartTime
	String :timestart
	String :timeend
	String :mapfile		# filename
	String :mapname		# mapname
	String :mapauthor	# author
	String :gametype	# internal type
	String :gamename	# human type
	String :mutators	# list of active mutators
	String :servername	# servername
	String :options		# server options
end

puts "creating table: match kills (m_kills)..." # Killdata for matrixes etc.
DB.create_table :m_kills do
	primary_key :id
	Int    :m_id		# MatchID
	Float  :stamp
	String :killer
	String :victim
	String :k_weap
	String :v_weap
end

puts "creating table: match stats (match_stats)..."
DB.create_table :match_stats do
	primary_key :id
	Int    :m_id		# MatchID
	Int    :p_id		# PlayerID
	Float  :join		# Join time
	Float  :part		# Part time
	String :p_name		# Name
	Int    :score		# v PLR STATS v
	Int    :frags		# TODO RM
	Int    :kills
	Int    :deths
	Int    :suics
	Int    :teamk
	Int    :teamd
	Int    :team		# ^-----------^
	Int    :firstblood	# TODO
	Float  :time
end


puts "creating table: players (players)..." # 
DB.create_table :players do
	primary_key :id
	Int    :first_game	# Career start 
	Int    :last_game	# Career until
	Int	   :p_id		# Player ID
	String :p_name		# Player Name
	Int    :score		# v PLR STATS v
	Int    :frags
	Int    :kills
	Int    :deths
	Int    :suics
	Int    :teamk
	Int    :teamd
	Int    :team		# ^-----------^
	Int    :matches
	Float  :time
end


puts "creating table: items (items)..." # Endless stream of item pickups
DB.create_table :items do
	primary_key :id
	Float  :stamp		# Timestamp
	Int    :m_id		# MatchID
	Int	   :p_id		# PlayerID
	String :item		# ItamPickedUp
end

puts "creating table: score (scores)..." # For graphing and possibly overriding default score values.
DB.create_table :scores do
	primary_key :id
	Int    :m_id		# MatchID
	Float  :stamp		# Timestamp
	Int	   :p_id		# PlayerID
	Float  :score		# Numeric value of score
	String :types		# Textual type of score (F=Flag_Capture, K=Kill)
end

puts "creating table: team score (scores_team)..." # For graphing and possibly overriding default score values.
DB.create_table :scores_team do
	primary_key :id
	Int    :m_id		# MatchID
	Float  :stamp		# Timestamp
	Int	   :t_id		# TeamID
	Float  :score		# Numeric value of score
	String :types		# Textual type of score (F=Flag_Capture, K=Kill)
end

puts "creating table: mapper_i  (map_items)..." # TODO -- is this even used?
DB.create_table :real_items do
	primary_key :id		# id		1
	String :item		# itemName	Flare
	String :item_frm	# itemName	UT2 Flare
end


puts "TODO -- ??totals etc"
DB.create_table :map_data do
	primary_key :id		# 
	String :mapfile		# filename
	String :mapname		# mapname
	String :mapauthor	# author
end




puts "TODO -- serverdata"

