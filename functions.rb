if PARSE_ON == 1 and INIT == 1
	puts "DB> CREATE"
	require './init_makedb.rb'
else
	puts "DB> NO CREATE"
end


def weapondata(wid)	# in: log weaponID, out: formatted name
	weap = Hash[
		"DamTypeRocket" 			=> "UT2 Rocket Launcher",
		"DamTypeAttackCraftPlasma" 	=> "UT2 Plasma",
		"ONSAttackCraft" 			=> "UT2 ONS Craft",
		"FlakCannon" 				=> "UT2 Flak Cannon"
	]
	return weap.fetch(wid,0)
end


@pidmap = Hash[]			# pidmap -- translated local pid into real pid'

def pid_append(loc,rea)		# in local_pid,real_pid | out: N/A | => append to translatable table
	@pidmap[loc] = rea
end

def pid_loc2rea(local_pid)	# in: local_pid | out: real_pid
	real_pid = @pidmap.fetch(local_pid,0)
	return real_pid
end

def getPID(name)	# in->name <=> out->PID (0 if new)
	myID = @players.where(:p_name => name).get(:id)
	if myID.to_s.length > 0
		puts "> #{name}(#{myID}) OK "
	else
		puts "> #{name} NEW!"
		myID = 0
	end
	return myID
end


def endgame_playertimes
	# TODO -- set endtime to gameend ONLY if part=-1 --> now re-joins will fail
	@pidmap.each do |v|
		mypid = v.fetch(1)
		DB.run("UPDATE match_stats SET part=\"#{$GAME_END}\" WHERE p_id=\"#{mypid}\" AND m_id = \"#{$THIS_MATCH}\" ")	
	end
end


def gameTime
	return ($GAME_END.to_i-$GAME_START.to_i) / 60
end


def playerdata(pid)
	return @players.where(:p_id => pid).get(:p_name)
end


def format_float(num)
	# fixes number (float) into formatted form => 2 digits
	precision = 2
	inte,deci = num.to_s.split('.')

	deci ||= "00"
	if deci.length != 2
		deci = deci+"0"
	end
	deci = deci[0,precision]
	num = inte+"."+deci

	return num
end


class String
	# fg 30-37
	def red;   "\e[31m#{self}\e[0m" end
	def green; "\e[32m#{self}\e[0m" end
	def yellow; "\e[33m#{self}\e[0m" end
	# bg 40-47
	def bg_black; "\e[40m#{self}\e[0m" end
	def bg_green; "\e[22m#{self}\e[0m" end
	# extra
	def bold;    "\e[1m#{self}\e[22m" end
	def italic;  "\e[3m#{self}\e[23m" end
	def under;   "\e[4m#{self}\e[24m" end
	def blink;   "\e[5m#{self}\e[25m" end
	def reverse; "\e[7m#{self}\e[27m" end
end

