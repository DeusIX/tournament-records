def interactive_mode
	puts "Command? (q = quit, h = help)"
	usercmd = ""

	until usercmd == "q" or usercmd == "quit"


		if usercmd == "h" or usercmd == "help"
			puts "[ HELP ]"
			puts "l, latest"
			puts "d, Match Details"
			puts "h, help: print help"
			puts "q, quit: quit ixUT"
			print "ixUT> "
			usercmd = gets.chomp

		elsif usercmd == "l"
			puts "[ Latest ]"
			output_latest_matches
			print "ixUT> "
			usercmd = gets.chomp

		elsif usercmd == "d"
			puts "[ Detail Match 1 ]"
			output_match(1)
			print "ixUT> "
			usercmd = gets.chomp

		else
			puts "[ ETC. ]"
			print "ixUT> "
			usercmd = gets.chomp
		end
	end

	puts "\n\"I am the Alpha and the Omega.\" - Xan Kriegor\n"
end


if INTERACTIVE != 0
	interactive_mode
end
